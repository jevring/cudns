package net.jevring.cudns;

import net.jevring.cudns.protocol.message.Header;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ResponseCodeException extends Exception {
	private final Header.ResponseCode responseCode;
	private final short id;

	public ResponseCodeException(String message, Header.ResponseCode responseCode, short id) {
		super(message);
		this.responseCode = responseCode;
		this.id = id;
	}

	public ResponseCodeException(String message, Throwable cause, Header.ResponseCode responseCode, short id) {
		super(message, cause);
		this.responseCode = responseCode;
		this.id = id;
	}

	public Header.ResponseCode getResponseCode() {
		return responseCode;
	}

	public short getId() {
		return id;
	}
}
