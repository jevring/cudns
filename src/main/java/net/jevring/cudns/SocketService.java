package net.jevring.cudns;

import net.jevring.cudns.protocol.Decoder;
import net.jevring.cudns.protocol.Encoder;
import net.jevring.cudns.protocol.message.Header;
import net.jevring.cudns.protocol.message.Message;
import net.jevring.cudns.service.DnsService;

import java.util.Collections;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class SocketService {
	private final DnsService service;
	private final Decoder decoder;
	private final Encoder encoder;

	public SocketService(DnsService service, Decoder decoder, Encoder encoder) {
		this.service = service;
		this.decoder = decoder;
		this.encoder = encoder;
	}

	public byte[] handle(byte[] rawMessage) {
		Message output;
		try {
			Message input = decoder.decode(rawMessage);
			output = service.handleQuery(input);
		} catch (ResponseCodeException e) {
			output = createErrorMessage(e);
			e.printStackTrace();
		}
		return encoder.encode(output);
	}

	private Message createErrorMessage(ResponseCodeException e) {
		return new Message(new Header(e.getId(), true, Header.OpCode.Query, false, false, false, true, (byte) 0, e.getResponseCode(), 0, 0, 0, 0),
		                   Collections.emptyList(),
		                   Collections.emptyList(),
		                   Collections.emptyList(),
		                   Collections.emptyList());
	}
}
