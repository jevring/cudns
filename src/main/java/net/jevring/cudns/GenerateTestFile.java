package net.jevring.cudns;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class GenerateTestFile {
	public static void main(String[] args) {
		try (FileOutputStream fos = new FileOutputStream("dnsperf.txt")) {
			for (int i = 0; i < 10_000_000; i++) {
				fos.write("pop.gmail.com A\n".getBytes(StandardCharsets.UTF_8));
			}
			fos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
