package net.jevring.cudns;

import net.jevring.cudns.protocol.Decoder;
import net.jevring.cudns.protocol.Encoder;
import net.jevring.cudns.service.Database;
import net.jevring.cudns.service.DnsService;
import net.jevring.cudns.service.Resolver;
import net.jevring.cudns.service.UdpDnsClient;
import net.jevring.cudns.socket.QueriesPerSecondReporter;
import net.jevring.cudns.socket.Service;
import net.jevring.cudns.socket.ThreadingModel;
import net.jevring.cudns.socket.tcp.NioTcpService;
import net.jevring.cudns.socket.tcp.OneThreadPerConnectionTcpService;
import net.jevring.cudns.socket.tcp.SingleThreadedTcpService;
import net.jevring.cudns.socket.udp.MultipleReceivingThreadsUdpService;
import net.jevring.cudns.socket.udp.NioUdpService;
import net.jevring.cudns.socket.udp.OneThreadPerConnectionUdpService;
import net.jevring.cudns.socket.udp.SingleThreadedUdpService;

import java.util.concurrent.*;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Main {
	/**
	 * todo: make this configurable. Alternatively have saner defaults.
	 */
	private static final String ROOT_NAME_SERVER = "8.8.8.8";

	public static void main(String[] args) {
		Decoder decoder = new Decoder();
		Encoder encoder = new Encoder();
		UdpDnsClient udpDnsClient = new UdpDnsClient(ROOT_NAME_SERVER, 53, encoder, decoder);
		Database database = new Database();
		loadMasterFile(database);
		DnsService service = new DnsService(new Resolver(udpDnsClient, database));
		SocketService socketService = new SocketService(service, decoder, encoder);
		QueriesPerSecondReporter queriesPerSecondReporter = new QueriesPerSecondReporter();


		ThreadingModel threadingModel = ThreadingModel.SingleThreaded;
		if (args.length > 0) {
			for (ThreadingModel model : ThreadingModel.values()) {
				if (model.name().equalsIgnoreCase(args[0])) {
					threadingModel = model;
					break;
				}
			}
		}

		Service udpService;
		Service tcpService;
		int port = 53;
		ExecutorService executor;
		switch (threadingModel) {
			case SingleThreaded:
				udpService = new SingleThreadedUdpService(queriesPerSecondReporter, socketService, port);
				tcpService = new SingleThreadedTcpService(socketService, port);
				break;
			case OneThreadPerConnection:
				executor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(),
				                                  Runtime.getRuntime().availableProcessors(),
				                                  60,
				                                  TimeUnit.SECONDS,
				                                  new LinkedBlockingQueue<>(),
				                                  new ThreadPoolExecutor.CallerRunsPolicy());
				udpService = new OneThreadPerConnectionUdpService(queriesPerSecondReporter, executor, socketService, port);
				tcpService = new OneThreadPerConnectionTcpService(executor, socketService, port);
				break;
			case MultipleReceiverThreads:
				int threads = Runtime.getRuntime().availableProcessors();
				//int threads = 1;
				executor = Executors.newFixedThreadPool(threads);
				udpService = new MultipleReceivingThreadsUdpService(queriesPerSecondReporter, executor, socketService, port, threads);
				tcpService = new OneThreadPerConnectionTcpService(executor, socketService, port);
				break;
			case EventDispatch:
				udpService = new NioUdpService(queriesPerSecondReporter, socketService, port);
				tcpService = new NioTcpService(queriesPerSecondReporter, socketService, port);
				break;
			// todo: are there other patterns to consider? would they just be version of event dispatch, like via a disruptor or something else like that?
			// todo: should we have a separate path for multiple selectors or other multi-threading hacks for event dispatch?
			default:
				udpService = new SingleThreadedUdpService(queriesPerSecondReporter, socketService, port);
				tcpService = new SingleThreadedTcpService(socketService, port);
				break;

		}
		ExecutorService serviceThreads = Executors.newFixedThreadPool(2);
		serviceThreads.execute(() -> runServiceContinuously(udpService));
		serviceThreads.execute(() -> runServiceContinuously(tcpService));
		queriesPerSecondReporter.start();
	}

	private static void runServiceContinuously(Service service) {
		while (true) {
			try {
				// todo: if we loop here, we'll try to bind again, which won't work, and we'll fail
				service.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	private static void loadMasterFile(Database database) {
		// todo: read the master file and build the database.

		// todo: don't read this zone into the normal tree. have an authoritative tree per zone. That way, when we read new zone info, we can atomically change the pointer to that zone
	}
}
