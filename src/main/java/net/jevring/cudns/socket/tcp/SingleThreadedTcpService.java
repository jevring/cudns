package net.jevring.cudns.socket.tcp;

import net.jevring.cudns.SocketService;
import net.jevring.cudns.socket.Service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class SingleThreadedTcpService implements Service {
	private final SocketService socketService;
	private final int port;

	public SingleThreadedTcpService(SocketService socketService, int port) {
		this.socketService = socketService;
		this.port = port;
	}

	public void start() {
		try (ServerSocket serverSocket = new ServerSocket(port)) {
			while (true) {
				try (Socket socket = serverSocket.accept()) {
					DataInputStream dis = new DataInputStream(socket.getInputStream());
					int length = dis.readUnsignedShort();
					byte[] buf = new byte[length];
					dis.readFully(buf);

					byte[] response = socketService.handle(buf);

					DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
					dos.writeShort(response.length);
					dos.write(response);
					dos.flush();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
