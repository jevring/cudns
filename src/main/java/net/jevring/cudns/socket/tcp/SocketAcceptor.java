package net.jevring.cudns.socket.tcp;

import net.jevring.cudns.SocketService;
import net.jevring.cudns.socket.QueriesPerSecondReporter;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.Executor;

/**
 * Accepts sockets and registers a request processor.
 *
 * @author markus@jevring.net
 */
public class SocketAcceptor implements Runnable {
	private final QueriesPerSecondReporter queriesPerSecondReporter;
	private final ServerSocketChannel serverSocketChannel;
	private final SocketService socketService;
	private final Selector selector;
	private final Executor executor;

	public SocketAcceptor(QueriesPerSecondReporter queriesPerSecondReporter,
	                      ServerSocketChannel serverSocketChannel,
	                      SocketService socketService,
	                      Selector selector,
	                      Executor executor) {
		this.queriesPerSecondReporter = queriesPerSecondReporter;
		this.serverSocketChannel = serverSocketChannel;
		this.socketService = socketService;
		this.selector = selector;
		this.executor = executor;
	}

	@Override
	public void run() {
		try {
			SocketChannel socketChannel = serverSocketChannel.accept();
			if (socketChannel == null) {
				return;
			}
			socketChannel.configureBlocking(false);

			// todo: use separate selectors for each processor? one new selector in one new thread, round-robin dispatch to these selectors/threads?
			// likely only useful when load on synchronized() becomes very high, if ever
			SelectionKey processorSelectorKey = socketChannel.register(selector, SelectionKey.OP_READ);
			SocketProcessor processor = new SocketProcessor(queriesPerSecondReporter, processorSelectorKey, socketChannel, socketService, selector, executor);
			processorSelectorKey.attach(processor);
			// wake up the selector so that it can take the new registration into account
			selector.wakeup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
