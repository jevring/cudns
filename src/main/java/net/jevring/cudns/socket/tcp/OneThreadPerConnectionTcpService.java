package net.jevring.cudns.socket.tcp;

import net.jevring.cudns.SocketService;
import net.jevring.cudns.socket.Service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class OneThreadPerConnectionTcpService implements Service {
	private final ExecutorService executorService;
	private final SocketService socketService;
	private final int port;

	public OneThreadPerConnectionTcpService(ExecutorService executorService, SocketService socketService, int port) {
		this.executorService = executorService;
		this.socketService = socketService;
		this.port = port;
	}

	public void start() {
		try (ServerSocket serverSocket = new ServerSocket(port)) {
			while (true) {
				Socket socket = serverSocket.accept();
				executorService.execute(new Runnable() {
					@Override
					public void run() {
						try (socket) {
							DataInputStream dis = new DataInputStream(socket.getInputStream());
							int length = dis.readUnsignedShort();
							byte[] buf = new byte[length];
							dis.readFully(buf);

							byte[] response = socketService.handle(buf);

							DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
							dos.writeShort(response.length);
							dos.write(response);
							dos.flush();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
