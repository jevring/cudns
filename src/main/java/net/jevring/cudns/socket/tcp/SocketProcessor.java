package net.jevring.cudns.socket.tcp;

import net.jevring.cudns.SocketService;
import net.jevring.cudns.socket.QueriesPerSecondReporter;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.concurrent.Executor;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class SocketProcessor implements Runnable {
	private final ByteBuffer input = ByteBuffer.allocate(4096);
	private final ByteBuffer output = ByteBuffer.allocate(4096);
	private final QueriesPerSecondReporter queriesPerSecondReporter;
	private final SelectionKey processorSelectorKey;
	private final SocketChannel socketChannel;
	private final SocketService socketService;
	private final Selector selector;
	private final Executor executor;

	public SocketProcessor(QueriesPerSecondReporter queriesPerSecondReporter,
	                       SelectionKey processorSelectorKey,
	                       SocketChannel socketChannel,
	                       SocketService socketService,
	                       Selector selector,
	                       Executor executor) {
		this.queriesPerSecondReporter = queriesPerSecondReporter;
		this.processorSelectorKey = processorSelectorKey;
		this.socketChannel = socketChannel;
		this.socketService = socketService;
		this.selector = selector;
		this.executor = executor;
	}

	@Override
	public void run() {
		try {
			if (processorSelectorKey.isReadable()) {
				read();
			} else if (processorSelectorKey.isWritable()) {
				write();
			}
		} catch (IOException e) {
			e.printStackTrace();
			processorSelectorKey.cancel();
		}
	}

	private void read() throws IOException {
		socketChannel.read(input);
		int position = input.position();
		if (position < 2) {
			// we can't read the length unless we have a short worth of data
			System.out.println("Waiting for 2 bytes so we can determine the length of the message");
			return;
		}
		// todo: how can we use the byte buffer directly for reads AND writes if we want to keep appending to it without flipping it back and forth and ruining it?
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(input.array()));
		int length = dis.readShort();
		if (length > input.position() - 2) {
			// the data isn't there yet. Wait for that to happen
			System.out.println("Waiting for the full set of data so we can get it all");
			return;
		}
		executor.execute(this::processAndSignalWrite);
	}

	private void processAndSignalWrite() {
		process();
		// todo: also check if we've used up the whole buffer and it needs to be extended
		processorSelectorKey.interestOps(SelectionKey.OP_WRITE);
		// wake up the selector so that it can take the new registration into account
		selector.wakeup();
	}

	private void write() throws IOException {
		socketChannel.write(output);
		queriesPerSecondReporter.hit();
		if (output.remaining() <= 0) {
			socketChannel.close();

			// we're done with this request. cancel the key and notify the selector
			processorSelectorKey.cancel();
			selector.wakeup();
		}
	}

	private void process() {
		input.flip();
		int length = input.getShort();
		byte[] buf = new byte[length];
		input.get(buf);
		byte[] response = socketService.handle(buf);
		output.putShort((short) response.length);
		output.put(response);
		output.flip();
	}
}
