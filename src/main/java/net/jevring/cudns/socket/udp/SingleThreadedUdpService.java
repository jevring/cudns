package net.jevring.cudns.socket.udp;

import net.jevring.cudns.SocketService;
import net.jevring.cudns.socket.QueriesPerSecondReporter;
import net.jevring.cudns.socket.Service;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class SingleThreadedUdpService implements Service {
	private final QueriesPerSecondReporter queriesPerSecondReporter;
	private final SocketService socketService;
	private final int port;

	public SingleThreadedUdpService(QueriesPerSecondReporter queriesPerSecondReporter, SocketService socketService, int port) {
		this.queriesPerSecondReporter = queriesPerSecondReporter;
		this.socketService = socketService;
		this.port = port;
	}

	public void start() {
		try (DatagramSocket socket = new DatagramSocket(port)) {
			byte[] buf = new byte[512];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			while (true) {
				try {
					socket.receive(packet);
					byte[] response = socketService.handle(buf);
					socket.send(new DatagramPacket(response, response.length, packet.getSocketAddress()));
					queriesPerSecondReporter.hit();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
