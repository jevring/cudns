package net.jevring.cudns.socket.udp;

import net.jevring.cudns.SocketService;
import net.jevring.cudns.socket.QueriesPerSecondReporter;
import net.jevring.cudns.socket.Service;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class NioUdpService implements Service {
	private final QueriesPerSecondReporter queriesPerSecondReporter;
	private final SocketService socketService;
	private final int port;

	public NioUdpService(QueriesPerSecondReporter queriesPerSecondReporter, SocketService socketService, int port) {
		this.queriesPerSecondReporter = queriesPerSecondReporter;
		this.socketService = socketService;
		this.port = port;
	}

	public void start() {
		ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() - 2);

		DatagramChannel channel = null;
		try {
			channel = DatagramChannel.open();
			channel.bind(new InetSocketAddress(port));
			channel.configureBlocking(false);

			Selector selector = Selector.open();
			SelectionKey rootKey = channel.register(selector, SelectionKey.OP_READ);
			rootKey.attach(new SocketProcessor(queriesPerSecondReporter, channel, socketService, rootKey, executor, selector));

			while (!Thread.interrupted()) {
				int numberOfSelectedKeys = selector.select(); // this blocks until there are things available to select. For non-blocking, use selectNow()
				if (numberOfSelectedKeys == 0) {
					continue;
				}
				Set<SelectionKey> selectionKeys = selector.selectedKeys();
				for (SelectionKey selectionKey : selectionKeys) {
					if (selectionKey.attachment() == null) {
						System.out.println("Attachment was null for selection key " + selectionKey);
						continue;
					}
					((Runnable) selectionKey.attachment()).run();
				}
				selectionKeys.clear();
			}
			rootKey.cancel();
			executor.shutdownNow();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (channel != null) {
				try {
					channel.close();
					channel.socket().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
