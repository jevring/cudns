package net.jevring.cudns.socket.udp;

import net.jevring.cudns.SocketService;
import net.jevring.cudns.socket.QueriesPerSecondReporter;
import net.jevring.cudns.socket.Service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.ExecutorService;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class OneThreadPerConnectionUdpService implements Service {
	private final QueriesPerSecondReporter queriesPerSecondReporter;
	private final ExecutorService executorService;
	private final SocketService socketService;
	private final int port;

	public OneThreadPerConnectionUdpService(QueriesPerSecondReporter queriesPerSecondReporter,
	                                        ExecutorService executorService,
	                                        SocketService socketService,
	                                        int port) {
		this.queriesPerSecondReporter = queriesPerSecondReporter;
		this.executorService = executorService;
		this.socketService = socketService;
		this.port = port;
	}

	public void start() {
		try (DatagramSocket socket = new DatagramSocket(port)) {
			while (true) {
				try {
					byte[] buf = new byte[512];
					DatagramPacket packet = new DatagramPacket(buf, buf.length);
					socket.receive(packet);
					executorService.execute(new Runnable() {
						@Override
						public void run() {
							try {
								byte[] response = socketService.handle(buf);
								socket.send(new DatagramPacket(response, response.length, packet.getSocketAddress()));
								queriesPerSecondReporter.hit();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
