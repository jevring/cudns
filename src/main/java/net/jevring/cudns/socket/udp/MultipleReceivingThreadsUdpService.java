package net.jevring.cudns.socket.udp;

import net.jevring.cudns.SocketService;
import net.jevring.cudns.socket.QueriesPerSecondReporter;
import net.jevring.cudns.socket.Service;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class MultipleReceivingThreadsUdpService implements Service {
	private final QueriesPerSecondReporter queriesPerSecondReporter;
	private final ExecutorService executorService;
	private final SocketService socketService;
	private final int port;
	private final int threads;

	public MultipleReceivingThreadsUdpService(QueriesPerSecondReporter queriesPerSecondReporter,
	                                          ExecutorService executorService,
	                                          SocketService socketService,
	                                          int port,
	                                          int threads) {
		this.queriesPerSecondReporter = queriesPerSecondReporter;
		this.executorService = executorService;
		this.socketService = socketService;
		this.port = port;
		this.threads = threads;
	}

	public void start() {
		for (int i = 0; i < threads; i++) {
			System.out.println("Spawning thread " + i);
			executorService.execute(new Runnable() {
				@Override
				public void run() {
					try (DatagramSocket socket = new DatagramSocket(null)) {
						socket.setReuseAddress(true);
						socket.bind(new InetSocketAddress(port));

						while (true) {
							byte[] buf = new byte[512];
							DatagramPacket packet = new DatagramPacket(buf, buf.length);
							socket.receive(packet);
							byte[] response = socketService.handle(buf);
							socket.send(new DatagramPacket(response, response.length, packet.getSocketAddress()));
							queriesPerSecondReporter.hit();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
		System.out.println("Done spawning threads, sleeping forever");
		try {
			Thread.sleep(Long.MAX_VALUE);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
