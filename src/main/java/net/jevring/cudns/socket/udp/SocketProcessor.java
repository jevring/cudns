package net.jevring.cudns.socket.udp;

import net.jevring.cudns.SocketService;
import net.jevring.cudns.socket.QueriesPerSecondReporter;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class SocketProcessor implements Runnable {
	private final Queue<UdpResponse> responses = new ConcurrentLinkedQueue<>();
	private final QueriesPerSecondReporter queriesPerSecondReporter;
	private final DatagramChannel datagramChannel;
	private final SocketService socketService;
	private final SelectionKey selectionKey;
	private final Executor executor;
	private final Selector selector;

	public SocketProcessor(QueriesPerSecondReporter queriesPerSecondReporter,
	                       DatagramChannel datagramChannel,
	                       SocketService socketService,
	                       SelectionKey selectionKey,
	                       Executor executor,
	                       Selector selector) {
		this.queriesPerSecondReporter = queriesPerSecondReporter;
		this.datagramChannel = datagramChannel;
		this.socketService = socketService;
		this.selectionKey = selectionKey;
		this.executor = executor;
		this.selector = selector;
	}

	@Override
	public void run() {
		// There's an ugly race condition here that I'm not sure how to get rid of.
		// If we receive a packet, process it, and change our interest to write, we start writing from the queue.
		// We then check if the queue is done, and change the interest to read. However, if we change the interest 
		// to read AFTER the new response has entered the queue but BEFORE we've had a change to read it AND no new
		// messages arrive, it'll be stuck in the queue forever, never getting sent, because we don't synchronize 
		// reader and writer (which, unfortunately, is the entire point).
		// It would be nice to solve this in some handy way, but since we don't have the concept of a connection with
		// separate interests, as in TCP, this is not trivial.
		// As long as the requests keep coming, however, that shouldn't be a problem. We can lie to ourselves and say
		// "this is UDP, some packet loss is expected", but that just wouldn't be right...
		try {
			if (selectionKey.isReadable()) {
				read();
			} else if (selectionKey.isWritable()) {
				write();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void read() throws IOException {
		ByteBuffer input = ByteBuffer.allocate(512);
		// will we need to even bother with incomplete messages, given that a packet is guaranteed to fit inside an ip packet?
		// likely not. Also, how could we, since UDP isn't session-based

		InetSocketAddress source = (InetSocketAddress) datagramChannel.receive(input);
		if (source != null) {
			executor.execute(() -> processRequestAndEnqueueResponse(input, source));
		}
	}

	private void processRequestAndEnqueueResponse(ByteBuffer input, InetSocketAddress source) {
		byte[] response = socketService.handle(input.array());
		ByteBuffer output = ByteBuffer.wrap(response);
		responses.offer(new UdpResponse(source, output));

		if ((selectionKey.interestOps() & SelectionKey.OP_WRITE) != SelectionKey.OP_WRITE) {
			selectionKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
			// wake up the selector so that it can take the new registration into account
			selector.wakeup();
		}
	}

	private void write() throws IOException {
		UdpResponse response = responses.poll();
		if (response != null) {
			int bytesToSend = response.getPacket().limit();
			int sentBytes = datagramChannel.send(response.getPacket(), response.getSource());
			if (sentBytes != bytesToSend) {
				// this should very rarely happen, and quite frankly, since this is UDP, we don't really care.
				System.err.printf("We only managed to send %d bytes out of %d%n", sentBytes, bytesToSend);
			}
			queriesPerSecondReporter.hit();
		} else {
			// we no longer have anything to write. stop trying, otherwise we'll enter here in a hot loop forever
			selectionKey.interestOps(SelectionKey.OP_READ);
			// wake up the selector so that it can take the new registration into account
			selector.wakeup();
		}
	}

	private static final class UdpResponse {
		private final InetSocketAddress source;
		private final ByteBuffer packet;

		private UdpResponse(InetSocketAddress source, ByteBuffer packet) {
			this.source = source;
			this.packet = packet;
		}

		private InetSocketAddress getSource() {
			return source;
		}

		private ByteBuffer getPacket() {
			return packet;
		}
	}

}
