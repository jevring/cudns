package net.jevring.cudns.socket;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class QueriesPerSecondReporter {
	private final AtomicInteger counter = new AtomicInteger();

	public void start() {
		Executors.newScheduledThreadPool(1).scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				int perSecond = counter.getAndSet(0);
				System.out.println(perSecond + " QPS");
			}
		}, 1, 1, TimeUnit.SECONDS);
	}

	public void hit() {
		counter.incrementAndGet();
	}
}
