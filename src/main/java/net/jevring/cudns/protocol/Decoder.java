package net.jevring.cudns.protocol;

import net.jevring.cudns.ResponseCodeException;
import net.jevring.cudns.protocol.message.Class;
import net.jevring.cudns.protocol.message.*;
import net.jevring.cudns.protocol.message.rdata.*;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Turns byte buffers into messages.
 *
 * @author markus@jevring.net
 */
public class Decoder {

	public Message decode(byte[] buf) throws ResponseCodeException {
		try {
			DataInputStream dis = new DataInputStream(new ByteArrayInputStream(buf));
			LabelParser labelParser = new LabelParser(dis, buf);
			return decode(dis, labelParser);
		} catch (IOException e) {
			throw new AssertionError("byte arrays can't throw IOExceptions", e);
		}
	}

	private Message decode(DataInputStream dis, LabelParser labelParser) throws IOException, ResponseCodeException {
		int id = dis.readUnsignedShort();
		try {
			Header header = parseHeader(dis, id);
			List<Question> question = parseQuestion(dis, header.getQdCount(), labelParser);
			List<ResourceRecord> answer = parseResourceRecord(dis, header.getAnCount(), labelParser);
			List<ResourceRecord> authority = parseResourceRecord(dis, header.getNsCount(), labelParser);
			List<ResourceRecord> additional = parseResourceRecord(dis, header.getArCount(), labelParser);
			return new Message(header, question, answer, authority, additional);
		} catch (RuntimeException e) {
			throw new ResponseCodeException(e.getMessage(), e, Header.ResponseCode.FormatError, (short) id);
		}
	}

	private List<Question> parseQuestion(DataInputStream dis, int qdCount, LabelParser labelParser) throws IOException {
		List<Question> questions = new ArrayList<>();
		for (int i = 0; i < qdCount; i++) {
			List<String> labels = labelParser.readLabelsAtCurrentLocation();
			Type qType = Type.forShort(dis.readShort());
			Class qClass = Class.forShort(dis.readShort());
			questions.add(new Question(labels, qType, qClass));
		}
		return questions;
	}

	private List<ResourceRecord> parseResourceRecord(DataInputStream dis, int count, LabelParser labelParser) throws IOException {
		List<ResourceRecord> records = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			List<String> labels = labelParser.readLabelsAtCurrentLocation();
			Type type = Type.forShort(dis.readShort());
			Class clazz = Class.forShort(dis.readShort());
			int ttl = dis.readInt();
			int rdLength = dis.readUnsignedShort();
			Optional<RData> rData = readRData(dis, rdLength, type, clazz, labelParser);
			if (rData.isPresent()) {
				records.add(new ResourceRecord(labels, type, clazz, ttl, rData.get()));
			}

		}
		return records;
	}

	private Optional<RData> readRData(DataInputStream dis, int rdLength, Type type, Class clazz, LabelParser labelParser) throws IOException {
		if (clazz != Class.IN) {
			// todo: this will throw all the way out. We'll need to have a general way of providing a correct error response.
			throw new IllegalArgumentException(String.format("Unsupported class: %s. Only supported class is IN", clazz));
		}
		switch (type) {
			case A:
				byte[] b = new byte[rdLength];
				dis.readFully(b);
				InetAddress ip = InetAddress.getByAddress(b);
				return Optional.of(new A(ip));
			case PTR:
			case CNAME:
			case NS:
			case MB:
			case MD:
			case MF:
			case MG:
			case MR:
				return Optional.of(new DomainName(labelParser.readLabelsAtCurrentLocation()));
			case TXT:
				return Optional.of(new CharacterString(parseCharacterString(dis)));
			case HINFO:
				return Optional.of(new HInfo(parseCharacterString(dis), parseCharacterString(dis)));
			case SOA:
				return Optional.of(new SOA(labelParser.readLabelsAtCurrentLocation(),
				                           labelParser.readLabelsAtCurrentLocation(),
				                           dis.readInt(),
				                           dis.readInt(),
				                           dis.readInt(),
				                           dis.readInt(),
				                           dis.readInt()));
			case MX:
				return Optional.of(new MX(dis.readShort(), labelParser.readLabelsAtCurrentLocation()));
			default:
				System.err.println(String.format("RData for %s and %s not yet supported", type, clazz));
				return Optional.empty();
		}
	}


	private Header parseHeader(DataInputStream dis, int id) throws IOException {

		byte bits1 = dis.readByte();
		boolean qr = (HeaderMasks.QR & bits1) == HeaderMasks.QR;
		byte opCode = (byte) ((HeaderMasks.OPCODE & bits1) >> 3);
		boolean aa = (HeaderMasks.AA & bits1) == HeaderMasks.AA;
		boolean tc = (HeaderMasks.TC & bits1) == HeaderMasks.TC;
		boolean rd = (HeaderMasks.RD & bits1) == HeaderMasks.RD;

		byte bits2 = dis.readByte();
		boolean ra = (HeaderMasks.RA & bits2) == HeaderMasks.RA;
		byte z = (byte) ((HeaderMasks.Z & bits2) >> 4);
		Header.ResponseCode rCode = Header.ResponseCode.forByte((byte) (HeaderMasks.RCODE & bits2));

		int qdCount = dis.readUnsignedShort();
		int anCount = dis.readUnsignedShort();
		int nsCount = dis.readUnsignedShort();
		int arCount = dis.readUnsignedShort();
		return new Header(id, qr, Header.OpCode.forByte(opCode), aa, tc, rd, ra, z, rCode, qdCount, anCount, nsCount, arCount);
	}

	private String parseCharacterString(DataInputStream dis) throws IOException {
		int length = dis.readUnsignedByte();
		byte[] bytes = new byte[length];
		dis.readFully(bytes);
		return new String(bytes, StandardCharsets.UTF_8);
	}
}
