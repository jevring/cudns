package net.jevring.cudns.protocol;

import net.jevring.cudns.protocol.message.Header;
import net.jevring.cudns.protocol.message.Message;
import net.jevring.cudns.protocol.message.Question;
import net.jevring.cudns.protocol.message.ResourceRecord;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Turns messages into byte buffers.
 *
 * @author markus@jevring.net
 */
public class Encoder {
	public byte[] encode(Message message) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream daos = new DataOutputStream(baos);
			writeHeader(daos, message.getHeader());
			writeQuestion(daos, message.getQuestion());
			writeResourceRecord(daos, message.getAnswer());
			writeResourceRecord(daos, message.getAuthority());
			writeResourceRecord(daos, message.getAdditional());
			return baos.toByteArray();
		} catch (IOException e) {
			throw new AssertionError("byte arrays don't throw IOExceptions", e);
		}
	}

	private void writeHeader(DataOutputStream daos, Header header) throws IOException {
		daos.writeShort(header.getId());
		byte bits1 = 0;
		if (header.isResponse()) {
			bits1 |= HeaderMasks.QR;
		}
		bits1 |= (header.getOpCode().getValue() << 1) & HeaderMasks.OPCODE;
		if (header.isAa()) {
			bits1 |= HeaderMasks.AA;
		}
		if (header.isTc()) {
			bits1 |= HeaderMasks.TC;
		}
		if (header.isRd()) {
			bits1 |= HeaderMasks.RD;
		}
		daos.writeByte(bits1);

		byte bits2 = 0;
		if (header.isRa()) {
			bits2 |= HeaderMasks.RA;
		}
		bits2 |= (header.getZ() << 4) & HeaderMasks.Z;
		bits2 |= header.getResponseCode().getValue() & HeaderMasks.RCODE;
		daos.writeByte(bits2);

		daos.writeShort(header.getQdCount());
		daos.writeShort(header.getAnCount());
		daos.writeShort(header.getNsCount());
		daos.writeShort(header.getArCount());
	}

	private void writeQuestion(DataOutputStream daos, List<Question> question) throws IOException {
		for (Question q : question) {
			writeDomainName(daos, q.getQname());
			daos.writeShort(q.getQType().getValue());
			daos.writeShort(q.getQClass().getValue());
		}
	}

	private void writeResourceRecord(DataOutputStream daos, List<ResourceRecord> resourceRecord) throws IOException {
		for (ResourceRecord record : resourceRecord) {
			writeDomainName(daos, record.getName());
			daos.writeShort(record.getType().getValue());
			daos.writeShort(record.getClazz().getValue());
			daos.writeInt(record.getTtl());
			record.getRData().write(daos);
		}
	}

	public static void writeDomainName(DataOutputStream daos, List<String> labels) throws IOException {
		for (String label : labels) {
			daos.writeByte(label.length());
			daos.write(label.getBytes(StandardCharsets.UTF_8));
			// fucking finally! it was this fucker all along!
			// I misread where it said that it wrote TWO octets of length instead of the desired 1
			//daos.writeUTF(label);
		}
		daos.writeByte(0);
	}
}
