package net.jevring.cudns.protocol;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public interface HeaderMasks {

	int QR =     0b10000000;
	int OPCODE = 0b01111000;
	int AA =     0b00000100;
	int TC =     0b00000010;
	int RD =     0b00000001;
	
	int RA =     0b10000000;
	int Z =      0b01110000;
	int RCODE =  0b00001111;
}
