package net.jevring.cudns.protocol;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads labels and handles compression pointers.
 *
 * @author markus@jevring.net
 */
public class LabelParser {
	private final DataInputStream dataInputStream;
	private final byte[] message;

	public LabelParser(DataInputStream dataInputStream, byte[] message) {
		this.dataInputStream = dataInputStream;
		this.message = message;
	}

	public List<String> readLabelsAtCurrentLocation() throws IOException {
		return readLabelsFromStream(dataInputStream);
	}

	private List<String> readLabelsFromStream(DataInputStream dataInputStream) throws IOException {
		byte length;
		List<String> labels = new ArrayList<>();
		while ((length = dataInputStream.readByte()) != 0) {
			String label;
			if (isCompressionPointer(length)) {
				// compression is on. Read the rest from the existing byte array.
				// this expects the whole rest of the list of labels to be at that offset, 
				// not just the next individual label 
				int labelByteOffset = length & 0b00111111;
				// RFC1035 doesn't specify that we need to read an extra byte to get the actual offset, but stack overflow is ever helpful =)
				// https://stackoverflow.com/questions/9865084/dns-response-answer-authoritative-section
				int totalOffset = labelByteOffset + dataInputStream.readByte();
				List<String> restOfTheLabels =
						readLabelsFromStream(new DataInputStream(new ByteArrayInputStream(message, totalOffset, message.length - totalOffset)));
				List<String> total = new ArrayList<>();
				total.addAll(labels);
				total.addAll(restOfTheLabels);
				return total;
			} else {
				byte[] labelBytes = new byte[length];
				dataInputStream.readFully(labelBytes);
				label = new String(labelBytes, StandardCharsets.UTF_8);
			}
			labels.add(label);

		}
		return labels;
	}

	private boolean isCompressionPointer(byte length) {
		return (length & 0b11000000) == 0b11000000;
	}
}
