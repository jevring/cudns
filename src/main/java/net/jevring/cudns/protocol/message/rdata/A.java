package net.jevring.cudns.protocol.message.rdata;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class A implements RData {
	private final InetAddress inetAddress;

	public A(InetAddress inetAddress) {
		this.inetAddress = inetAddress;
	}

	public InetAddress getInetAddress() {
		return inetAddress;
	}

	@Override
	public void write(DataOutputStream daos) throws IOException {
		byte[] address = inetAddress.getAddress();
		daos.writeShort(address.length);
		daos.write(address);
	}

	@Override
	public String toString() {
		return "A{" + "inetAddress=" + inetAddress + '}';
	}
}
