package net.jevring.cudns.protocol.message;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Header {
	private final int id; // 16 bits, just bits, not a number
	private final boolean qr; // 1 bit
	private final OpCode opCode; // 4 bits
	private final boolean aa; // 1 bit
	private final boolean tc; // 1 bit
	private final boolean rd; // 1 bit
	private final boolean ra; // 1 bit
	private final byte z; // 3 bits, must all be 0
	private final ResponseCode responseCode; // 4 bits
	private final int qdCount; // 16 bits, unsigned
	private final int anCount; // 16 bits, unsigned
	private final int nsCount; // 16 bits, unsigned
	private final int arCount; // 16 bits, unsigned

	public Header(int id,
	              boolean qr,
	              OpCode opCode,
	              boolean aa,
	              boolean tc,
	              boolean rd,
	              boolean ra,
	              byte z,
	              ResponseCode responseCode,
	              int qdCount,
	              int anCount,
	              int nsCount,
	              int arCount) {
		this.id = id;
		this.qr = qr;
		this.opCode = opCode;
		this.aa = aa;
		this.tc = tc;
		this.rd = rd;
		this.ra = ra;
		this.z = z;
		this.responseCode = responseCode;
		this.qdCount = qdCount;
		this.anCount = anCount;
		this.nsCount = nsCount;
		this.arCount = arCount;
	}

	public boolean isQuery() {
		return !qr;
	}

	public boolean isResponse() {
		return qr;
	}

	public int getId() {
		return id;
	}

	public OpCode getOpCode() {
		return opCode;
	}

	public boolean isAa() {
		return aa;
	}

	public boolean isTc() {
		return tc;
	}

	public boolean isRd() {
		return rd;
	}

	public boolean getRd() {
		return rd;
	}

	public boolean isRa() {
		return ra;
	}

	public byte getZ() {
		return z;
	}

	public ResponseCode getResponseCode() {
		return responseCode;
	}

	public int getQdCount() {
		return qdCount;
	}

	public int getAnCount() {
		return anCount;
	}

	public int getNsCount() {
		return nsCount;
	}

	public int getArCount() {
		return arCount;
	}

	@Override
	public String toString() {
		return "Header{" + "id=" + id + ", qr=" + qr + ", opCode=" + opCode + ", aa=" + aa + ", tc=" + tc + ", rd=" + rd + ", ra=" + ra + ", z=" + z + ", rCode=" + responseCode + ", qdCount=" + qdCount + ", anCount=" + anCount + ", nsCount=" + nsCount + ", arCount=" + arCount + '}';
	}

	public enum OpCode {
		Query(0),
		InverseQuery(1),
		Status(2);

		private final byte value;

		OpCode(int value) {
			this.value = (byte) value;
		}

		public static OpCode forByte(byte b) {
			switch (b) {
				case 0:
					return Query;
				case 1:
					return InverseQuery;
				case 2:
					return Status;
				default:
					throw new IllegalArgumentException("Unknown opcode: " + b);
			}
		}

		public byte getValue() {
			return value;
		}
	}

	public enum ResponseCode {
		Ok(0),
		FormatError(1),
		ServerFailure(2),
		NameError(3),
		NotImplemented(4),
		Refused(5);

		private final byte value;

		ResponseCode(int value) {
			this.value = (byte) value;
		}

		public static ResponseCode forByte(byte b) {
			switch (b) {
				case 0:
					return Ok;
				case 1:
					return FormatError;
				case 2:
					return ServerFailure;
				case 3:
					return NameError;
				case 4:
					return NotImplemented;
				case 5:
					return Refused;
				default:
					throw new IllegalArgumentException("Unknown rcode: " + b);
			}
		}

		public byte getValue() {
			return value;
		}
	}

}
