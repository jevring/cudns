package net.jevring.cudns.protocol.message.rdata;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class HInfo implements RData {
	private final String cpu;
	private final String os;

	public HInfo(String cpu, String os) {
		this.cpu = cpu;
		this.os = os;
	}

	@Override
	public void write(DataOutputStream daos) throws IOException {
		byte[] cpuBytes = cpu.getBytes(StandardCharsets.UTF_8);
		byte[] osBytes = os.getBytes(StandardCharsets.UTF_8);
		daos.writeShort(1 + cpuBytes.length + 1 + osBytes.length);
		daos.writeByte(cpuBytes.length);
		daos.write(cpuBytes);

		daos.writeByte(osBytes.length);
		daos.write(osBytes);
	}

	@Override
	public String toString() {
		return "HInfo{" + "cpu='" + cpu + '\'' + ", os='" + os + '\'' + '}';
	}
}
