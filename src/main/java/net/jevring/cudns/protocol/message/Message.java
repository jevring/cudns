package net.jevring.cudns.protocol.message;

import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Message {
	public static final boolean QR_RESPONSE = true;
	public static final boolean ALWAYS_WILLING_TO_RECURSE = true;

	private final Header header;
	private final List<Question> question;
	private final List<ResourceRecord> answer;
	private final List<ResourceRecord> authority;
	private final List<ResourceRecord> additional;

	public Message(Header header, List<Question> question, List<ResourceRecord> answer, List<ResourceRecord> authority, List<ResourceRecord> additional) {
		this.header = header;
		this.question = question;
		this.answer = answer;
		this.authority = authority;
		this.additional = additional;
	}

	public Header getHeader() {
		return header;
	}

	public List<Question> getQuestion() {
		return question;
	}

	public List<ResourceRecord> getAnswer() {
		return answer;
	}

	public List<ResourceRecord> getAuthority() {
		return authority;
	}

	public List<ResourceRecord> getAdditional() {
		return additional;
	}

	@Override
	public String toString() {
		return "Message{" + "header=" + header + ", \nquestion=" + question + ", \nanswer=" + answer + ", \nauthority=" + authority + ", \nadditional=" + additional + '}';
	}
}
