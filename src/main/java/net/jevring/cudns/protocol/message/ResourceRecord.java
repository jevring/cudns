package net.jevring.cudns.protocol.message;

import net.jevring.cudns.protocol.message.rdata.RData;

import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class ResourceRecord {
	/**
	 * This is a sequence of domain name labels.
	 * todo: are there circumstances under which this isn't the case?
	 */
	private final List<String> name;
	private final Type type;
	private final Class clazz;
	private final int ttl; // signed 32 bit integer
	private final RData rData;

	public ResourceRecord(List<String> name, Type type, Class clazz, int ttl, RData rData) {
		this.name = name;
		this.type = type;
		this.clazz = clazz;
		this.ttl = ttl;
		this.rData = rData;
	}

	public List<String> getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public Class getClazz() {
		return clazz;
	}

	public int getTtl() {
		return ttl;
	}

	public RData getRData() {
		return rData;
	}

	@Override
	public String toString() {
		return "ResourceRecord{" + "name=" + name + ", type=" + type + ", clazz=" + clazz + ", ttl=" + ttl + ", rData=" + rData + '}';
	}
}
