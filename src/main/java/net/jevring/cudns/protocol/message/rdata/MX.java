package net.jevring.cudns.protocol.message.rdata;

import net.jevring.cudns.protocol.Encoder;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class MX implements RData {
	private final short preference;
	private final List<String> exchange;

	public MX(short preference, List<String> exchange) {
		this.preference = preference;
		this.exchange = exchange;
	}

	@Override
	public void write(DataOutputStream daos) throws IOException {
		daos.writeShort(exchange.size() + exchange.stream().mapToInt(String::length).sum() + 1 + 2);
		daos.writeShort(preference);
		Encoder.writeDomainName(daos, exchange);
	}

	@Override
	public String toString() {
		return "MX{" + "preference=" + preference + ", exchange=" + exchange + '}';
	}
}
