package net.jevring.cudns.protocol.message.rdata;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public interface RData {
	public void write(DataOutputStream daos) throws IOException;
}
