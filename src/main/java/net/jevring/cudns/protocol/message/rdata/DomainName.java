package net.jevring.cudns.protocol.message.rdata;

import net.jevring.cudns.protocol.Encoder;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class DomainName implements RData {
	private final List<String> labels;

	public DomainName(List<String> labels) {
		this.labels = labels;
	}

	public List<String> getLabels() {
		return labels;
	}

	@Override
	public void write(DataOutputStream daos) throws IOException {
		daos.writeShort(labels.size() + labels.stream().mapToInt(String::length).sum() + 1);
		Encoder.writeDomainName(daos, labels);
	}

	@Override
	public String toString() {
		return "DomainName{" + "labels=" + labels + '}';
	}
}
