package net.jevring.cudns.protocol.message;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public enum Type {
	A(1),
	NS(2),
	MD(3),
	MF(4),
	CNAME(5),
	SOA(6),
	MB(7),
	MG(8),
	MR(9),
	NULL(10),
	WKS(11),
	PTR(12),
	HINFO(13),
	MINFO(14),
	MX(15),
	TXT(16),
	/**
	 * Only when used as a QTYPE.
	 */
	AXFR(252),
	/**
	 * Only when used as a QTYPE.
	 */
	MAILB(253),
	/**
	 * Only when used as a QTYPE.
	 */
	MAILA(254),
	/**
	 * Only when used as a QTYPE.
	 */
	STAR(255);

	private final short value;

	Type(int value) {
		this.value = (short) value;
	}

	public static Type forShort(short i) {
		// todo: don't do this with a loop, since values() clones the array each time
		for (Type type : values()) {
			if (type.value == i) {
				return type;
			}
		}
		throw new IllegalArgumentException("Can't map value " + i);
	}

	public short getValue() {
		return value;
	}
}
