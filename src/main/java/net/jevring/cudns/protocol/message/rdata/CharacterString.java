package net.jevring.cudns.protocol.message.rdata;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class CharacterString implements RData {
	private final String characterString;

	public CharacterString(String characterString) {
		this.characterString = characterString;
	}

	@Override
	public void write(DataOutputStream daos) throws IOException {
		byte[] bytes = characterString.getBytes(StandardCharsets.UTF_8);
		daos.writeShort(bytes.length + 1);
		daos.writeByte(bytes.length);
		daos.write(bytes);
	}

	@Override
	public String toString() {
		return "CharacterString{" + "characterString='" + characterString + '\'' + '}';
	}
}
