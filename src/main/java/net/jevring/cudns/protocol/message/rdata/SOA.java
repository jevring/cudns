package net.jevring.cudns.protocol.message.rdata;

import net.jevring.cudns.protocol.Encoder;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class SOA implements RData {
	private final List<String> mname;
	private final List<String> rname;
	private final int serial; // 32 bit UNSIGNED integer
	private final int refresh; // 32 bit UNSIGNED integer
	private final int retry; // 32 bit UNSIGNED integer
	private final int expire; // 32 bit UNSIGNED integer
	private final int minimum; // 32 bit UNSIGNED integer

	public SOA(List<String> mname, List<String> rname, int serial, int refresh, int retry, int expire, int minimum) {
		this.mname = mname;
		this.rname = rname;
		this.serial = serial;
		this.refresh = refresh;
		this.retry = retry;
		this.expire = expire;
		this.minimum = minimum;
	}

	@Override
	public void write(DataOutputStream daos) throws IOException {
		int mnameSize = mname.size() + mname.stream().mapToInt(String::length).sum() + 1;
		int rnameSize = rname.size() + rname.stream().mapToInt(String::length).sum() + 1;
		int intsSize = 5 * 4;
		daos.writeShort(mnameSize + rnameSize + intsSize);

		Encoder.writeDomainName(daos, mname);
		Encoder.writeDomainName(daos, rname);

		daos.writeInt(serial);
		daos.writeInt(refresh);
		daos.writeInt(retry);
		daos.writeInt(expire);
		daos.writeInt(minimum);
	}

	@Override
	public String toString() {
		return "SOA{" + "mname=" + mname + ", rname=" + rname + ", serial=" + serial + ", refresh=" + refresh + ", retry=" + retry + ", expire=" + expire + ", minimum=" + minimum + '}';
	}
}
