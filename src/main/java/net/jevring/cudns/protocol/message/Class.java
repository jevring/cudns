package net.jevring.cudns.protocol.message;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public enum Class {
	IN(1),
	CS(2),
	CH(3),
	HS(4),
	/**
	 * Only valid when used as QCLASS
	 */
	STAR(255);

	private final short value;

	Class(int value) {
		this.value = (short) value;
	}

	public static Class forShort(short i) {
		// todo: don't do this with a loop, since values() clones the array each time
		for (Class clazz : values()) {
			if (clazz.value == i) {
				return clazz;
			}
		}
		throw new IllegalArgumentException("Can't map value " + i);
	}

	public short getValue() {
		return value;
	}
}
