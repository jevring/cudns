package net.jevring.cudns.protocol.message;

import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Question {
	/**
	 * A list of labels, starting from the smallest, such as: ["www", "google", "com"]
	 * todo: handle compression when the first two bits of the length octets are 11
	 */
	private final List<String> qname;
	private final Type qType; // 16 bits
	private final Class qClass; // 16 bits

	public Question(List<String> qname, Type qType, Class qClass) {
		this.qname = qname;
		this.qType = qType;
		this.qClass = qClass;
	}

	public List<String> getQname() {
		return qname;
	}

	public Type getQType() {
		return qType;
	}

	public Class getQClass() {
		return qClass;
	}

	@Override
	public String toString() {
		return "Question{" + "qname=" + qname + ", qType=" + qType + ", qClass=" + qClass + '}';
	}
}
