package net.jevring.cudns.service;

import net.jevring.cudns.protocol.message.Class;
import net.jevring.cudns.protocol.message.ResourceRecord;
import net.jevring.cudns.protocol.message.Type;
import net.jevring.cudns.protocol.message.rdata.A;
import net.jevring.cudns.protocol.message.rdata.DomainName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Database {
	private final Node root = new Node();

	public Node findNode(List<String> labels) {
		Node current = root;
		for (String label : tldFirst(labels)) {
			current = current.getChildOrStar(label.toLowerCase());
			if (current == null) {
				return null;
			}
		}
		return current;
	}

	private Node getOrCreateNode(List<String> name) {
		Node current = root;
		for (String label : tldFirst(name)) {
			current = current.getOrCreateChild(label.toLowerCase());
		}
		return current;
	}

	public void store(List<ResourceRecord> resourceRecords) {
		for (ResourceRecord resourceRecord : resourceRecords) {
			List<String> name = resourceRecord.getName();
			Node node = getOrCreateNode(name);
			// todo: make sure to store this under "*" if it was a wildcard.
			// ideally that should happen automatically by the list of labels ending in "*", but double-check
			node.addResourceRecord(resourceRecord);
			if (resourceRecord.getRData() instanceof A) {
				A a = (A) resourceRecord.getRData();
				String[] addressOctets = a.getInetAddress().getHostAddress().split("\\.");
				List<String> reverseName = Arrays.asList(addressOctets[3], addressOctets[2], addressOctets[1], addressOctets[0], "IN-ADDR", "ARPA");
				Node reverseNode = getOrCreateNode(reverseName);
				reverseNode.addResourceRecord(new ResourceRecord(reverseName, Type.PTR, Class.IN, resourceRecord.getTtl(), new DomainName(name)));
			}
		}
	}

	/**
	 * Reverses a name so that the TLD is first. We do this here, as all names we work with, from input
	 * to response from other name servers are NOT TLD first.
	 */
	private List<String> tldFirst(List<String> name) {
		List<String> reversed = new ArrayList<>(name);
		Collections.reverse(reversed);
		return reversed;
	}
}
