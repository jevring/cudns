package net.jevring.cudns.service;

import net.jevring.cudns.protocol.message.ResourceRecord;
import net.jevring.cudns.protocol.message.Type;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Node {
	private final Map<Type, List<TtlWrapper<ResourceRecord>>> data = new ConcurrentHashMap<>();
	private final Map<String, Node> children = new ConcurrentHashMap<>();

	public Node getOrCreateChild(String label) {
		return children.computeIfAbsent(label, l -> new Node());
	}

	// todo: if we find nothing, we have to walk back up instead of a star to match...
	public Node getChildOrStar(String label) {
		Node node = children.get(label);
		if (node == null) {
			node = children.get("*");
		}
		return node;
	}

	public List<ResourceRecord> getResourceRecords(Type type) {
		if (type == Type.STAR) {
			// todo: this should also take wildcards into account. If this is a wildcard, find all children BELOW it and return them as well
			List<ResourceRecord> records = new ArrayList<>();
			for (List<TtlWrapper<ResourceRecord>> dataForType : data.values()) {
				dataForType.removeIf(TtlWrapper::isExpired);
				dataForType.stream().map(TtlWrapper::getValue).forEach(records::add);
			}
			return records;
		} else {
			List<TtlWrapper<ResourceRecord>> dataForType = data.computeIfAbsent(type, ignored -> new CopyOnWriteArrayList<>());
			dataForType.removeIf(TtlWrapper::isExpired);
			return dataForType.stream().map(TtlWrapper::getValue).collect(Collectors.toList());
		}
	}

	public void addResourceRecord(ResourceRecord resourceRecord) {
		List<TtlWrapper<ResourceRecord>> dataForType = data.computeIfAbsent(resourceRecord.getType(), ignored -> new CopyOnWriteArrayList<>());
		dataForType.removeIf(TtlWrapper::isExpired);
		Iterator<TtlWrapper<ResourceRecord>> iterator = dataForType.iterator();
		while (iterator.hasNext()) {
			TtlWrapper<ResourceRecord> ttlWrapper = iterator.next();
			ResourceRecord value = ttlWrapper.getValue();

			if (sameRecord(value, resourceRecord)) {
				// this exact record will be added again below, but with a new ttl
				iterator.remove();
			}
		}
		dataForType.add(new TtlWrapper<>(expirationTimestampInMilliseconds(resourceRecord.getTtl()), resourceRecord));
	}

	private boolean sameRecord(ResourceRecord value, ResourceRecord resourceRecord) {
		return value.getType() == resourceRecord.getType() &&
		       value.getClazz() == resourceRecord.getClazz() &&
		       value.getName().equals(resourceRecord.getName()) &&
		       value.getRData().equals(resourceRecord.getRData());
	}

	private long expirationTimestampInMilliseconds(int ttl) {
		return System.currentTimeMillis() + (ttl * 1000);
	}

	private static final class TtlWrapper<T> {
		private final long expirationTimestampInMilliseconds;
		private final T value;

		private TtlWrapper(long expirationTimestampInMilliseconds, T value) {
			this.expirationTimestampInMilliseconds = expirationTimestampInMilliseconds;
			this.value = value;
		}

		private boolean isExpired() {
			return expirationTimestampInMilliseconds < System.currentTimeMillis();
		}

		private T getValue() {
			return value;
		}

		@Override
		public String toString() {
			return "TtlWrapper{" + ", value=" + value + "expirationTimestampInMilliseconds=" + expirationTimestampInMilliseconds + '}';
		}
	}
}
