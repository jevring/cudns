package net.jevring.cudns.service;

import net.jevring.cudns.ResponseCodeException;
import net.jevring.cudns.protocol.Decoder;
import net.jevring.cudns.protocol.Encoder;
import net.jevring.cudns.protocol.message.Header;
import net.jevring.cudns.protocol.message.Message;
import net.jevring.cudns.protocol.message.Question;
import net.jevring.cudns.protocol.message.ResourceRecord;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class UdpDnsClient {
	private final AtomicInteger idGenerator = new AtomicInteger();
	private final Encoder encoder;
	private final Decoder decoder;
	private final InetSocketAddress server;

	public UdpDnsClient(String host, int port, Encoder encoder, Decoder decoder) {
		this.server = new InetSocketAddress(host, port);
		this.encoder = encoder;
		this.decoder = decoder;
	}

	public List<ResourceRecord> query(Question question) throws ResponseCodeException {
		short id = generateId();
		Header header = new Header(id, false, Header.OpCode.Query, false, false, true, false, (byte) 0, Header.ResponseCode.Ok, 1, 0, 0, 0);
		Message message = new Message(header, Collections.singletonList(question), Collections.emptyList(), Collections.emptyList(), Collections.emptyList());
		System.out.println("Sending question to parent: " + question);

		try {
			DatagramSocket socket = new DatagramSocket();
			byte[] buf = encoder.encode(message);
			socket.send(new DatagramPacket(buf, buf.length, server));
			byte[] responseBuf = new byte[512];
			socket.receive(new DatagramPacket(responseBuf, 512));
			Message response = decoder.decode(responseBuf);
			if (response.getHeader().getId() != id) {
				System.err.printf("Got the wrong id back. Got %s but expected %s for message %s%n", response.getHeader().getId(), id, message);
				return null;
			}
			return response.getAnswer();
		} catch (IOException e) {
			System.err.println("Could not ask parent for message " + message);
			e.printStackTrace();
		}
		return Collections.emptyList();
	}

	private short generateId() {
		// todo: we can't roll this over atomically (for short). Let's fix this in some nice way
		// actually, it probably "rolls" just fine as we truncate down to short.
		// alternatively we could just hash the requested domain name down to a short. but then we might get collisions.
		return (short) idGenerator.incrementAndGet();


	}
}
