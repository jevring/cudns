package net.jevring.cudns.service;

import net.jevring.cudns.ResponseCodeException;
import net.jevring.cudns.protocol.message.Question;
import net.jevring.cudns.protocol.message.ResourceRecord;
import net.jevring.cudns.protocol.message.Type;
import net.jevring.cudns.protocol.message.rdata.DomainName;

import java.util.ArrayList;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class Resolver {
	private final UdpDnsClient udpDnsClient;
	private final Database database;

	public Resolver(UdpDnsClient udpDnsClient, Database database) {
		this.udpDnsClient = udpDnsClient;
		this.database = database;
	}

	public List<ResourceRecord> resolve(Question question, boolean queryParent) throws ResponseCodeException {
		List<ResourceRecord> allResourceRecords = new ArrayList<>();
		Node node = database.findNode(question.getQname());
		boolean askParent = false;
		if (node == null) {
			askParent = queryParent;
		} else {
			List<ResourceRecord> resourceRecords = node.getResourceRecords(question.getQType());

			if (resourceRecords.isEmpty()) {
				List<ResourceRecord> cNameRecords = node.getResourceRecords(Type.CNAME);
				allResourceRecords.addAll(cNameRecords);
				for (ResourceRecord cNameRecord : cNameRecords) {
					// todo: pass along a Set<List<Name>>, so we can check for CNAME loops

					// todo: resolve these, eventually (hopefully) getting down to the desired record (like A or MX)
					Question cNameQuestion = new Question(((DomainName) cNameRecord.getRData()).getLabels(), question.getQType(), question.getQClass());
					List<ResourceRecord> innerCNameRecords = resolve(cNameQuestion, queryParent);
					allResourceRecords.addAll(innerCNameRecords);
				}
				askParent = !questionIsAnswered(question, allResourceRecords);
			} else {
				allResourceRecords.addAll(resourceRecords);
			}
		}
		if (askParent) {
			List<ResourceRecord> parentRecords = udpDnsClient.query(question);
			database.store(parentRecords);

			//allResourceRecords.addAll(parentRecords);
			// always hit our own cache, just to be sure that it works
			allResourceRecords.addAll(resolve(question, false));
		}
		if (allResourceRecords.isEmpty()) {
			System.err.printf("Missing resource record of type %s in %s%n", question.getQType(), question.getQname());
		}
		return allResourceRecords;
	}

	private boolean questionIsAnswered(Question question, List<ResourceRecord> allResourceRecords) {
		// we since we might have a question for example.com A, but we have example.com CNAME -> hello.com, hello.com >, 
		// we can't check if the exact question has been answered, but rather if we have something that might look like a final answer.
		// todo: this seems a bit flaky. can we do better?
		return allResourceRecords.stream().anyMatch(rr -> rr.getType() == question.getQType() && rr.getClazz() == question.getQClass());
	}
}
