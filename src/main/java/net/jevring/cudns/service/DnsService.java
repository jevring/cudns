package net.jevring.cudns.service;

import net.jevring.cudns.ResponseCodeException;
import net.jevring.cudns.protocol.message.Class;
import net.jevring.cudns.protocol.message.*;

import java.util.ArrayList;
import java.util.List;

import static net.jevring.cudns.protocol.message.Message.ALWAYS_WILLING_TO_RECURSE;
import static net.jevring.cudns.protocol.message.Message.QR_RESPONSE;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class DnsService {
	private final Resolver resolver;

	public DnsService(Resolver resolver) {
		this.resolver = resolver;
	}

	public Message handleQuery(Message input) throws ResponseCodeException {
		Header inputHeader = input.getHeader();
		if (!inputHeader.isQuery()) {
			throw new ResponseCodeException("Query was tagged as response", Header.ResponseCode.FormatError, (short) inputHeader.getId());
		}
		List<ResourceRecord> answer = new ArrayList<>();
		List<ResourceRecord> authority = new ArrayList<>();
		List<ResourceRecord> additional = new ArrayList<>();
		for (Question question : input.getQuestion()) {
			if (question.getQClass() != Class.IN) {
				throw new ResponseCodeException("We only support class IN, not " + question.getQClass(),
				                                Header.ResponseCode.NotImplemented,
				                                (short) inputHeader.getId());
			}
			answer.addAll(resolver.resolve(question, inputHeader.isRd()));
		}
		List<Question> question = input.getQuestion();
		boolean authoritativeAnswer = !authority.isEmpty(); // this can only be true if the data comes from our master file
		boolean truncated = false; // todo: how can we determine this before we generate the message?
		byte z = inputHeader.getZ();
		Header header = new Header(inputHeader.getId(),
		                           QR_RESPONSE,
		                           inputHeader.getOpCode(),
		                           authoritativeAnswer,
		                           truncated,
		                           inputHeader.getRd(),
		                           ALWAYS_WILLING_TO_RECURSE,
		                           z,
		                           Header.ResponseCode.Ok,
		                           question.size(),
		                           answer.size(),
		                           authority.size(),
		                           additional.size());
		return new Message(header, question, answer, authority, additional);
	}
}
