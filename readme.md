# cudns

A [dns](https://tools.ietf.org/html/rfc1035) server 


Test with `dig @127.0.0.1 +norecurse +noadflag +noedns +besteffort www.google.com`
This adflag stuff turns Z from all 0 into 2.

This DNS server will, in addition to actually serving DNS requests, serve as a socket and multi-threading playground. As such it probably won't be anything resembling "best practises", but that also isn't the point =)

# Single threaded UDP
```
$ resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
DNS Resolution Performance Testing Tool
Nominum Version 2.0.0.0

[Status] Command line: resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
[Status] Sending
[Status] Fell behind by 1000 queries, ending test at 38366 qps
[Status] Waiting for more responses
[Status] Testing complete

Statistics:

  Queries sent:         440586
  Queries completed:    440586
  Queries lost:         0
  Run time (s):         100.000002
  Maximum throughput:   29582.000000 qps
  Lost at that point:   0.00%

```

# One thread per connection UDP
Threads: Runtime.getRuntime().availableProcessors() - 2
```
$ resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
DNS Resolution Performance Testing Tool
Nominum Version 2.0.0.0

[Status] Command line: resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
[Status] Sending
[Status] Fell behind by 1000 queries, ending test at 29213 qps
[Status] Waiting for more responses
[Status] Testing complete

Statistics:

  Queries sent:         255027
  Queries completed:    255027
  Queries lost:         0
  Run time (s):         100.000000
  Maximum throughput:   27290.000000 qps
  Lost at that point:   0.42%

```

# One thread per connection UDP
Threads: 1
```
$ resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
DNS Resolution Performance Testing Tool
Nominum Version 2.0.0.0

[Status] Command line: resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
[Status] Sending
[Status] Fell behind by 1006 queries, ending test at 35197 qps
[Status] Waiting for more responses
[Status] Testing complete

Statistics:

  Queries sent:         370646
  Queries completed:    370646
  Queries lost:         0
  Run time (s):         100.000001
  Maximum throughput:   33772.000000 qps
  Lost at that point:   0.00%
```

# Event dispatch UDP
Threads: Runtime.getRuntime().availableProcessors() - 2
```
$ resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
DNS Resolution Performance Testing Tool
Nominum Version 2.0.0.0

[Status] Command line: resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
[Status] Sending
[Status] Reached 65536 outstanding queries
[Status] Waiting for more responses
[Status] Testing complete

Statistics:

  Queries sent:         276364
  Queries completed:    276364
  Queries lost:         0
  Run time (s):         100.000002
  Maximum throughput:   29532.000000 qps
  Lost at that point:   0.00%
```

# Multiple receiving threads UDP
Threads: Runtime.getRuntime().availableProcessors() - 2
```
$ resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
DNS Resolution Performance Testing Tool
Nominum Version 2.0.0.0

[Status] Command line: resperf -d dnsperf.txt -s 127.0.0.1 -m 100000 -r 60
[Status] Sending
[Status] Fell behind by 1000 queries, ending test at 29517 qps
[Status] Waiting for more responses
[Status] Testing complete

Statistics:

  Queries sent:         260370
  Queries completed:    260370
  Queries lost:         0
  Run time (s):         100.000000
  Maximum throughput:   27714.000000 qps
  Lost at that point:   0.02%
```

Nothing I do makes it go any faster with multiple threads than with a single thread. =(